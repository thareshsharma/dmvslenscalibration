#include "laser_dot_detector_2d.h"
#include <opencv2/core.hpp>

int main()
{
	const auto image = cv::imread(R"(..\ExampleImages\grey0_CVRawMatrix_0013.png)", cv::IMREAD_UNCHANGED);

	// Displays the loaded image
	cv::imshow("image", image);
	cv::waitKey(100);

	// detect dots in raw image coordinates
	std::vector<cv::Vec2f> dots0;

	// For dot brightness, to separate bright from dark dots
	std::vector<cv::Vec6f> dots_infos0;

	LaserDotDetector2D::Params params;
	params.bg_limit = 10;

	LaserDotDetector2D ldd0(image, params);
	ldd0.FindLaserDots(dots0, &dots_infos0);

	for(auto dot0 : dots0)
	{
		std::cout << "Dot (x,y) = " << dot0[0] << " " << dot0[1] << std::endl;
	}

	for(auto dot_info0 : dots_infos0)
	{
		std::cout << "Dot Infos = " << dot_info0[0] << " " << dot_info0[1] << " " << dot_info0[2] << " " << dot_info0[3] << " " << dot_info0[4] << " "
			<< dot_info0[5] << std::endl;
	}
}
